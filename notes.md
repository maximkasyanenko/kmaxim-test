How to install chocolatey:
https://chocolatey.org/docs/installation#install-with-powershellexe

### powershell improvements

```
choco install poshgit
```

Add the following to your $PROFILE
```
Import-Module C:\tools\poshgit\dahlbyk-posh-git-9bda399\src\posh-git.psd1
```



choco install git
choco install vagrant
choco install font-awesome-font

# Vagrant

Allow vagrant to read from .env file

```
vagrant plugin install vagrant-env
```

How to update the machine when synced_folders have changed:
```
vagrant reload
```

How to re-provision my machine (re-run provision steps in `Vagrantfile`):
```
vagrant up --provision
```

How to list all running vagrant machines:
``` 
vagrant global-status
```

How to delete a vm:
```
vagrant destroy {machine_id}
```


## Helpful articles:
https://www.nickhammond.com/configuring-vagrant-virtual-machines-with-env/