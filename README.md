# Vagrant Boxes

A project for all of Paradigm developer's virtual workstations. Setup and usage is documented in the [Windows Workstation page in the Wiki](http://wtswiki.wtsparadigm.com/index.php/Windows_Workstation).