#########################
# zsh                   #
#########################
export ZSH=$HOME/.oh-my-zsh
ZSH_THEME="robbyrussell"
# auto load .nvmrc and apply when cd into a directory that has an .nvmrc
# this must be loaded before the zsh-nvm plugin
export NVM_AUTO_USE=true
plugins=(
    git
    docker
    vi-mode
    archlinux
    zsh-autosuggestions
    # custom plugins #
    # https://github.com/lukechilds/zsh-nvm
    zsh-nvm)
source $ZSH/oh-my-zsh.sh
#########################
# zsh                   #
#########################
# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='vim'
else
  export EDITOR='nvim'
fi
# Compilation flags
export ARCHFLAGS="-arch x86_64"

export PATH=$PATH:$HOME/.local/bin
alias h="cd ~"
alias ez="vim ~/.zshrc"
alias vz="vim ~/.zshrc"
alias sz="source ~/.zshrc"
alias gs="git status"
alias gau="git add -u" # git add unstaged only
alias gaa="git add -A" # git add all
alias gcm="git commit -m"
alias gca="git commit --amend"
alias gb="git branch"
alias gd="git diff"
alias gds="git diff --staged"
alias gcb="git checkout -b"
alias gc="git commit --verbose"
alias gbl="git branch -l"
alias gp="git push"
alias gpf="git push --force-with-lease"
alias gpp="quick-git-check-in"
alias glv="git log | vim -"
alias gl="git log"
# git push and set upstream to current branch
function push_upstream () {
    git push -u origin $(git branch | grep "*" | awk -F " " '{print $NF}')
}
alias gpu=push_upstream
alias sctl="sudo systemctl"
alias pbcopy="xclip -selection clipboard"
alias pbpaste="xclip -selection clipboard -o"
alias restart="shutdown -r now"
alias pacman="sudo pacman"
alias x="chmod +x"
alias tools="cd ~/.local/bin/tools/ && ll"
alias c="cd ~/code && ll"
alias cgbb="cd ~/code/go/src/bitbucket.org/wtsdevops && ll"
alias cggh="cd $GOPATH/src/github.com/$GITHUB_ACCOUNT && ll"
alias vssh="vim ~/.ssh/config"
alias lssh="ls ~/.ssh"
alias rmrf="rm -rfi"
alias ranger='ranger --choosedir=$HOME/.rangerdir; LASTDIR=`cat $HOME/.rangerdir`; cd "$LASTDIR"'
alias v='vim'
update_golang() {
    # update golang pacman package
    echo "\nUpdating golang...\n"
    sudo pacman -Sy --needed go
    echo "\nUpdating golang packages...\n"
    go get -u all
}
function generate_password() {
    password=$(< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c 32; echo;)
    echo $password | pbcopy
    echo "New password copied to clipboard."
}
alias passgen=generate_password
# Re-run ansible that configures the box.
# Mostly equivalent to 'vagrant up --provision' or 'vagrant provision'
source /etc/profile.d/myvars.sh
alias update_box="ansible-playbook ~/code/vagrant-boxes/vagrant.yml --extra-vars \"git_email=$GIT_EMAIL git_name=$GIT_NAME\""
## CUSTOM KEY BINDINGS ##
## zsh vi-mode settings
# remaps ESC to fd
bindkey -M viins 'fd' vi-cmd-mode
bindkey 'lk' autosuggest-accept

## Azure
if [[ -f /home/$USER/.local/bin/azure-cli/az.completion ]]; then
    autoload bashcompinit && bashcompinit
    source /home/$USER/.local/bin/azure-cli/az.completion
fi

## VIM POWERLINE
if [[ -r ~/.local/lib/python2.7/site-packages/powerline/bindings/zsh/powerline.zsh ]]; then
    source ~/.local/lib/python2.7/site-packages/powerline/bindings/zsh/powerline.zsh
fi

export ANSIBLE_PLAYBOOKS_DIR=~/code/ansible-playbooks
export VAULT_ADDR=https://vault.wts-ops.com/
# export EDITOR=nvim
# alias vim=nvim

# Allow users to import their own rc file
if [[ -f ~/.rc ]]; then
    source ~/.rc
fi