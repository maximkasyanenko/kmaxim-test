Vagrant.configure("2") do |config|
  config.env.enable # Enable vagrant-env(.env)
  config.vm.box = "bento/ubuntu-18.04"
  config.vm.network "forwarded_port", guest: 8080, host: 8080
  config.ssh.forward_agent = true
  config.vm.synced_folder ENV['CODE_DIR'], "/home/vagrant/code", mount_options: ["dmode=775"]

  $set_environment_variables = <<SCRIPT
tee "/etc/profile.d/myvars.sh" > "/dev/null" <<EOF
export GIT_EMAIL="#{ENV['GIT_EMAIL']}"
export GIT_NAME="#{ENV['GIT_NAME']}"
EOF
SCRIPT
  config.vm.provision "shell", inline: $set_environment_variables, run: "always"

  config.vm.provider "virtualbox" do |v|
    v.memory = ENV['BOX_MEM']
    v.cpus = ENV['BOX_CPUS']
  end

  config.vm.provision "ansible_local" do |ansible|
    ansible.playbook = "vagrant.yml"
    ansible.version = "2.8.4"
    ansible.install_mode = "pip"
    ansible.extra_vars = {
      git_email: ENV['GIT_EMAIL'],
      git_name: ENV['GIT_NAME']
    }
    ansible.provisioning_path = "/home/vagrant/code/vagrant-boxes"
  end

end
